import React, { useEffect } from "react";

import styles from "./index.css";

import { Factory } from "./visualizer/factory";
import { Player, Tracks } from "./visualizer/player";

interface Props {
  tracks: Tracks[];
  nextSong?: () => void;
  prevSong?: () => void;
}

export default ({ tracks, nextSong, prevSong }: Props) => {
  useEffect(() => {
    const playerInstance = Factory.getInstance("player") as Player;
    playerInstance.songTracks = tracks;
    if (nextSong) {
      playerInstance.nextSongCallback = nextSong;
    }

    if (prevSong) {
      playerInstance.prevSongCallback = prevSong;
    }

    playerInstance.init();
  });

  return (
    <>
      <div className={styles.player}>
        <canvas></canvas>
        <div className={styles.song}>
          <div className={styles.artist} id="artist" />
          <div className={styles.name} id="songName" />
        </div>
        <div className={styles.playarea}>
          <div className={styles.prevSong} id="prevSong"></div>
          <div className={styles.play} id="play"></div>
          <div className={styles.pause} id="pause"></div>
          <div className={styles.nextSong} id="nextSong"></div>
        </div>
        <div className={styles.soundControl} id="soundControl"></div>
        <div className={styles.time} id="time">
          00:00
        </div>
      </div>
    </>
  );
};
