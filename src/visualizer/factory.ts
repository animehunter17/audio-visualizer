import {Framer} from './framer'
import {Tracker} from './tracker'
import {Scene} from './scene'
import {Player} from './player'
import {Controls} from './controls'

export class Factory {
  static framer: Framer;
  static tracker: Tracker;
  static scene: Scene;
  static player: Player;
  static controls: Controls;

  static getInstance(className: string) {
    const classname = className.toLowerCase()

    if (classname === "framer") {
      if (!this.framer) {
        this.framer = new Framer();
      }

      return this.framer;
    }

    if (classname === "tracker") {
      if (!this.tracker) {
        this.tracker = new Tracker();
      }

      return this.tracker;
    }

    if (classname === "scene") {
      if (!this.scene) {
        this.scene = new Scene();
      }

      return this.scene;
    }
    if (classname === "controls") {
      if (!this.controls) {
        this.controls = new Controls();
      }

      return this.controls;
    }

    if (classname === "player") {
      if (!this.player) {
        this.player = new Player();
      }

      return this.player;
    }

    return null
  }
}
