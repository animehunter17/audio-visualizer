import { Factory } from "./factory";
import { Scene } from "./scene";
import { Player } from "./player";

export class Tracker {
  innerDelta: number = 20;
  lineWidth: number = 7;
  prevAngle: number = 0.5;
  angle: number = 0;
  animationCount: number = 10;
  pressButton: boolean = false;
  scene: Scene;
  context: CanvasRenderingContext2D | null;
  animatedInProgress: boolean | undefined = false;
  player: Player;
  animateId: NodeJS.Timeout;
  radius: number

  init(scene: Scene) {
    this.scene = scene;
    this.context = scene.context;
    this.player = Factory.getInstance("player") as Player;
    this.initHandlers();
  }

  initHandlers() {
    const that = this;

    this.scene.canvas!.addEventListener("mousedown", (e: MouseEvent) => {
      if (that.isInsideOfSmallCircle(e) || that.isOutsideOfBigCircle(e)) {
        return;
      }

      that.prevAngle = that.angle;
      that.pressButton = true;
      that.stopAnimation();
      that.calculateAngle(e, true);
    });

    window.addEventListener("mouseup", () => {
      if (!that.pressButton) {
        return;
      }
      const id = setInterval(() => {
        if (!that.animatedInProgress) {
          that.pressButton = false;
          clearInterval(id);
        }
      }, 100);
    });

    window.addEventListener("mousemove", (e: MouseEvent) => {
      if (that.animatedInProgress) {
        return;
      }
      if (that.pressButton && that.scene.inProcess()) {
        that.calculateAngle(e);
      }
    });
  }

  isInsideOfSmallCircle(e: MouseEvent) {
    const x = Math.abs(e.pageX - this.scene.cx - this.scene.coord.left);
    const y = Math.abs(e.pageY - this.scene.cy - this.scene.coord.top);
    return Math.sqrt(x * x + y * y) < this.scene.radius - 3 * this.innerDelta;
  }

  isOutsideOfBigCircle(e: MouseEvent) {
    return (
      Math.abs(e.pageX - this.scene.cx - this.scene.coord.left) >
        this.scene.radius ||
      Math.abs(e.pageY - this.scene.cy - this.scene.coord.top) >
        this.scene.radius
    );
  }

  draw() {
    const playerSource = this.player.getSource();
    if (!playerSource.buffer) {
      return;
    }

    if (!this.pressButton) {
      this.angle =
        (this.player.context.currentTime / playerSource.buffer.duration) *
          2 *
          Math.PI || 0;
    }
    this.drawArc();
  }

  drawArc() {
    if (this.context) {
      this.context.save();
      this.context.strokeStyle = "rgba(254, 67, 101, 0.8)";
      this.context.beginPath();
      this.context.lineWidth = this.lineWidth;

      this.radius = this.scene.radius - (this.innerDelta + this.lineWidth / 2);

      this.context.arc(
        this.scene.radius + this.scene.padding,
        this.scene.radius + this.scene.padding,
        this.radius,
        0,
        this.angle,
        false
      );
      this.context.stroke();
      this.context.restore();
    }
  }

  calculateAngle(e: MouseEvent, animatedInProgress?: boolean) {
    this.animatedInProgress = animatedInProgress;
    const mx = e.pageX;
    const my = e.pageY;
    this.angle = Math.atan(
      (my - this.scene.cy - this.scene.coord.top) /
        (mx - this.scene.cx - this.scene.coord.left)
    );
    if (mx < this.scene.cx + this.scene.coord.left) {
      this.angle = Math.PI + this.angle;
    }
    if (this.angle < 0) {
      this.angle += 2 * Math.PI;
    }
    if (animatedInProgress) {
      this.startAnimation();
    } else {
      this.prevAngle = this.angle;
    }
  }

  startAnimation() {
    const that = this;
    const angle = this.angle;
    const l = Math.abs(this.angle) - Math.abs(this.prevAngle);
    let step = l / this.animationCount,
      i = 0;
    const f = () => {
      that.angle += step;
      if (++i == that.animationCount) {
        that.angle = angle;
        that.prevAngle = angle;
        that.animatedInProgress = false;
      } else {
        that.animateId = setTimeout(f, 20);
      }
    };

    this.angle = this.prevAngle;
    this.animateId = setTimeout(f, 20);
  }

  stopAnimation() {
    clearTimeout(this.animateId);
    this.animatedInProgress = false;
  }
}
