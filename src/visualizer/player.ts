import { Factory } from "./factory";
import { Framer } from "./framer";

export interface Tracks {
  artist: string;
  song: string;
  url: string;
}

export class Player {
  duration: number = 0;
  tracks: Tracks[] = [];
  context: AudioContext;
  firstLaunch: boolean = false;
  gainNode: GainNode;
  analyser: AnalyserNode;
  source: AudioBufferSourceNode;
  javascriptNode: ScriptProcessorNode;
  framer: Framer;
  currentSongIndex: number = 0;
  nextTrackCallback: () => void | null;
  prevTrackCallback: () => void | null;

  constructor() {
    this.framer = Factory.getInstance("framer") as Framer;
  }

  init() {
    this.firstLaunch = true;
    try {
      this.loadTrack(0);
    } catch (e) {
      this.framer.setLoadingPercent(1);
    }
    this.framer.setLoadingPercent(1);
    Factory.getInstance("scene");
  }

  set songTracks(tracks: Tracks[]) {
    this.tracks = tracks;
  }

  get currentTime() {
    return this.context.currentTime;
  }

  getSource() {
    return this.source;
  }

  set nextSongCallback(callback: () => void) {
    this.nextTrackCallback = callback
  }

  set prevSongCallback(callback: () => void) {
    this.prevTrackCallback = callback
  }

  loadTrack(index: number) {
    this.context = new AudioContext();
    this.context.suspend && this.context.suspend();

    this.javascriptNode = this.context.createScriptProcessor(2048, 1, 1);
    this.javascriptNode.connect(this.context.destination);
    this.analyser = this.context.createAnalyser();
    this.analyser.connect(this.javascriptNode);
    this.analyser.smoothingTimeConstant = 0.6;
    this.analyser.fftSize = 2048;
    this.source = this.context.createBufferSource();
    const destination = this.context.destination;
    this.gainNode = this.context.createGain();
    this.source.connect(this.gainNode);
    this.gainNode.connect(this.analyser);
    this.gainNode.connect(destination);

    const that = this;
    const request = new XMLHttpRequest();
    const track = that.tracks[index];

    document.getElementById("artist")!.textContent = track.artist;
    document.getElementById("songName")!.textContent = track.song;

    this.currentSongIndex = index;

    request.open("GET", track.url, true);
    request.responseType = "arraybuffer";

    request.onload = function() {
      that.context.decodeAudioData(request.response, (buffer: AudioBuffer) => {
        that.source.buffer = buffer;
        if (!that.firstLaunch) {
          that.source.start();
        }
      });
    };

    request.send();
    this.initHandlers();
  }

  nextTrack() {
    this.source.stop();
    this.context.suspend();

    ++this.currentSongIndex;
    if (this.currentSongIndex == this.tracks.length) {
      this.currentSongIndex = 0;
    }

    this.loadTrack(this.currentSongIndex);

    if (this.nextTrackCallback) {
      this.nextTrackCallback()
    }
  }

  prevTrack() {
    if (this.currentSongIndex === 0) {
      return;
    }

    this.source.stop();
    this.context.suspend();
    --this.currentSongIndex;

    this.loadTrack(this.currentSongIndex);

    if (this.prevTrackCallback) {
      this.prevTrackCallback()
    }
  }

  play() {
    this.context.resume && this.context.resume();

    if (this.firstLaunch) {
      this.source.start();
      this.firstLaunch = false;
    }
  }

  pause() {
    this.context.suspend();
  }

  mute() {
    this.gainNode.gain.value = 0;
  }

  unmute() {
    this.gainNode.gain.value = 1;
  }

  initHandlers() {
    const that = this;

    this.javascriptNode.onaudioprocess = function() {
      that.framer.frequencyData = new Uint8Array(
        that.analyser.frequencyBinCount
      );
      that.analyser.getByteFrequencyData(that.framer.frequencyData);
    };
  }
}
