import { Factory } from "./factory";
import { Player } from "./player";
import { Scene } from "./scene";
import { Tracker} from './tracker'

export class Controls {
  playing: boolean = false;
  scene: Scene;
  context: CanvasRenderingContext2D | null;
  timeControl: HTMLElement | null;
  playButton: HTMLElement | null;
  pauseButton: HTMLElement | null;
  soundButton: HTMLElement | null;
  player: Player;
  tracker: Tracker;

  init(scene: Scene) {
    this.scene = scene;
    this.context = scene.context;
    this.initHandlers();
    this.timeControl = document.getElementById("time");
    this.player = Factory.getInstance("player") as Player;
    this.tracker = Factory.getInstance("tracker") as Tracker;
  }

  initHandlers() {
    this.initPlayButton();
    this.initPauseButton();
    this.initSoundButton();
    this.initPrevSongButton();
    this.initNextSongButton();
    this.initTimeHandler();
  }

  initPlayButton() {
    const that = this;
    this.playButton = document.getElementById("play");
    this.playButton!.addEventListener("mouseup", function() {
      that.playButton!.style.display = "none";
      that.pauseButton!.style.display = "inline-block";
      that.player.play();
      that.playing = true;
    });
  }

  initPauseButton() {
    const that = this;
    this.pauseButton = document.getElementById("pause");
    this.pauseButton!.addEventListener("mouseup", function() {
      that.playButton!.style.display = "inline-block";
      that.pauseButton!.style.display = "none";
      that.player.pause();
      that.playing = false;
    });
  }

  initSoundButton() {
    const that = this;
    this.soundButton = document.getElementById("soundControl");
    this.soundButton!.addEventListener("mouseup", function() {
      if (that.soundButton!.classList.contains("disable")) {
        that.soundButton!.classList.remove("disable");
        that.player.unmute();
      } else {
        that.soundButton!.classList.add("disable");
        that.player.mute();
      }
    });
  }

  initPrevSongButton() {
    const that = this;
    const prevSongButton = document.getElementById("prevSong");
    prevSongButton!.addEventListener("mouseup", function() {
      that.player.prevTrack();
      that.playing && that.player.play();
    });
  }

  initNextSongButton() {
    const that = this;
    const nextSongButton = document.getElementById("nextSong");
    nextSongButton!.addEventListener("mouseup", function() {
      that.player.nextTrack();
      that.playing && that.player.play();
    });
  }

  initTimeHandler() {
    const that = this;
    setTimeout(() => {
      const rawTime = that.player.context.currentTime || 0;
      const secondsInMin = 60;
      let min: string | number = Math.floor(rawTime / secondsInMin);
      let seconds: number | string = Math.floor(rawTime - min * secondsInMin);

      if (min < 10) {
        min = `0${min}`;
      }
      if (seconds < 10) {
        seconds = `0${seconds}`;
      }

      const time = min + ":" + seconds;
      that.timeControl!.textContent = time;
      that.initTimeHandler();
    }, 300);
  }

  draw() {
    this.drawPic();
  }

  drawPic() {
    if (this.context) {
      this.context.save();
      this.context.beginPath();
      this.context.fillStyle = "rgba(254, 67, 101, 0.85)";
      this.context.lineWidth = 1;
      let x =
        this.tracker.radius /
        Math.sqrt(Math.pow(Math.tan(this.tracker.angle), 2) + 1);
      let y = Math.sqrt(this.tracker.radius * this.tracker.radius - x * x);
      if (this.getQuadrant() == 2) {
        x = -x;
      }
      if (this.getQuadrant() == 3) {
        x = -x;
        y = -y;
      }
      if (this.getQuadrant() == 4) {
        y = -y;
      }
      this.context.arc(
        this.scene.radius + this.scene.padding + x,
        this.scene.radius + this.scene.padding + y,
        10,
        0,
        Math.PI * 2,
        false
      );
      this.context.fill();
      this.context.restore();
    }
  }

  getQuadrant(): number | null {
    const angle = this.tracker.angle;
    if (0 <= angle && angle < Math.PI / 2) {
      return 1;
    }
    if (Math.PI / 2 <= angle && angle < Math.PI) {
      return 2;
    }
    if (Math.PI < angle && angle < (Math.PI * 3) / 2) {
      return 3;
    }
    if ((Math.PI * 3) / 2 <= angle && angle <= Math.PI * 2) {
      return 4;
    }

    return null;
  }
}
