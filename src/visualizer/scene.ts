import {Factory} from './factory'
import {Framer} from './framer'
import {Tracker} from './tracker'
import {Controls} from './controls'

export class Scene {
  padding: number = 120;
  minSize: number = 740;
  optimiseHeight: number = 982;
  _inProcess: boolean = false;
  framer: Framer;
  tracker: Tracker;
  controls: Controls;
  canvas: HTMLCanvasElement | null;
  context: CanvasRenderingContext2D | null;
  scaleCoef: number;
  width: number = 0;
  height: number = 0;
  radius: number;
  cx: number;
  cy: number;
  coord: DOMRect;

  constructor() {
    this.canvasConfigure();
    this.initHandlers();

    this.framer = Factory.getInstance('framer') as Framer;
    this.framer.init(this);
    this.tracker = Factory.getInstance('tracker') as Tracker;
    this.tracker.init(this);
    this.controls = Factory.getInstance('controls') as Controls;
    this.controls.init(this);

    this.startRender();
  }

  canvasConfigure() {
    this.canvas = document.querySelector("canvas");
    this.context = this.canvas!.getContext("2d");
    this.context!.strokeStyle = "#FE4365";
    this.calculateSize();
  }

  calculateSize() {
    this.scaleCoef = Math.max(0.5, 740 / this.optimiseHeight);

    const size = Math.max(this.minSize, 1 /*document.body.clientHeight */);
    this.canvas!.setAttribute("width", size.toString());
    this.canvas!.setAttribute("height", size.toString());
    //this.canvas.style.marginTop = -size / 2 + 'px';
    //this.canvas.style.marginLeft = -size / 2 + 'px';

    this.width = size;
    this.height = size;

    this.radius = (size - this.padding * 2) / 2;
    this.cx = this.radius + this.padding;
    this.cy = this.radius + this.padding;
    this.coord = this.canvas!.getBoundingClientRect();
  }

  initHandlers() {
    const that = this;
    window.onresize = function() {
      that.canvasConfigure();
      that.framer.configure();
      that.render();
    };
  }

  render() {
    const that = this;
    requestAnimationFrame(() => {
      that.clear();
      that.draw();
      if (that._inProcess) {
        that.render();
      }
    });
  }

  clear() {
    this.context!.clearRect(0, 0, this.width, this.height);
  }

  draw() {
    this.framer.draw();
    this.tracker.draw();
    this.controls.draw();
  }

  startRender() {
    this._inProcess = true;
    this.render();
  }

  stopRender() {
    this._inProcess = false;
  }

  inProcess() {
    return this._inProcess;
  }
}


