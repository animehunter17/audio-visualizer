## Installation

Simply enter:

```
npm install
```

## Usage

```
import Visualizer from '@ryusenpai/react-audio-visualizer'

const tracks = [
  {
    artist: 'artist name',
    song: 'song name',
    url: 'url to music'
  }
]

export function App() {
  return <Visualizer tracks={tracks} />
}
```
